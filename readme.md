[swiss.py][github_pages]
========================
[![GitHub version][github_badge]][github_tags] [![Build status][travis_badge]][travis] [![Code coverage][codecov_badge]][codecov] [![PyPI version][pypi_badge]][pypi]

_Creating a pocket sized toolkit library for any situation._

Documentation
-------------
If you would like to understand what this library offers please refer to the [API][github_pages].

Purpose
-------
Hacking together libraries from all over the place can be tiring.
Also, the knowledge gained from the creation of a library is beneficial.
Both of these factors have lead to the development of the utilities in this self-contained library.

License
-------
Copyright © Mr Axilus.
This project is licensed under [CC BY-NC-SA 4.0][license].

[codecov]: https://codecov.io/github/mraxilus/swiss.py
[codecov_badge]: https://img.shields.io/codecov/c/github/mraxilus/swiss.py.svg
[github_badge]: https://img.shields.io/github/release/mraxilus/swiss.py.svg
[github_pages]: http://mr.axilus.name/swiss.py/
[github_tags]: https://github.com/mraxilus/swiss.py/tags
[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[pypi]: https://pypi.python.org/pypi/swisspy/
[pypi_badge]: https://img.shields.io/pypi/v/swisspy.svg
[travis]: https://travis-ci.org/mraxilus/swiss.py
[travis_badge]: https://img.shields.io/travis/mraxilus/swiss.py.svg
